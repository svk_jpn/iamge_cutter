extern crate photon_rs;
use std::env;
use std::fs;
use std::path::{Path};
use photon_rs::native::{open_image, save_image};
use photon_rs::transform::{crop};
use rand::Rng;

fn main() {
    let args:Vec<String> = env::args().collect();
    if args.len() < 5 {
        println!("Usage : image_cutter [path to image] [width] [height] [nums]");
        return;
    }
    // 引数のパース
    let src_path = Path::new(&args[1]);
    let width:u32 = args[2].parse().unwrap();
    let height:u32 = args[3].parse().unwrap();
    let nums:u32 = args[4].parse().unwrap();
    // 画像のオープン
    if let Some(img_path) = src_path.to_str() {
        // 画像をオープン
        let mut img = open_image(img_path).unwrap();
        // 画像名と同じディレクトリを作成
        let mut img_name = "";
        if let Some(temp) = src_path.file_name() {
            if let Some(fname) = temp.to_str() {
                let temp:Vec<&str>  = fname.split('.').collect();
                img_name = temp[0];
                fs::create_dir(img_name).unwrap();
            }
        }
        // オフセット範囲の計算
        let offset_h_max = img.get_width() as i32 - width as i32;
        let offset_v_max = img.get_height() as i32 - height as i32;
        if offset_h_max < 0 || offset_v_max < 0 {
            println!("クロップサイズ({}, {})は画像サイズ({},{})より小さくして下さい。"
                , width, height, offset_h_max, offset_v_max
            );
            return;
        }
        // 画像をランダムクロップして保存
        let offset_h_max = offset_h_max as u32;
        let offset_v_max = offset_v_max as u32;
        let mut rng = rand::thread_rng();
        for n in 0..nums {
            // 切り出しオフセットの算出
            let offset_x = rng.gen_range(0..=offset_h_max);
            let offset_y = rng.gen_range(0..=offset_v_max);
            let img_crop =  crop(&mut img, 
                offset_x, offset_y, offset_x + width, offset_y + height);
            let save_path = format!("{}/image_{:>05}.png", img_name, n);
            save_image(img_crop, &save_path).unwrap();
        }
    }
    else {
        println!("画像のパスが不正です");
    }
}
